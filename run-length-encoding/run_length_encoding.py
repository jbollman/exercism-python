# original dictionary approach.
# need a dictionary

def encode(string):
    key_list = []
    d = {}
    if string:
        for char in string:
            if char not in d:
                d[char] = 1
            else:
                d[char] += 1
        for key in d.keys():
            if d[key] == 1:
                d[key] = ''
            key_list += str(d[key]) + key
        return "".join(key_list)
    else:
        return ''
    

def decode(string):
    string = string