from datetime import timedelta
def add_gigasecond(bd):
    return bd + timedelta(seconds=10**9)
